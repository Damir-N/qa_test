from selenium import webdriver

driver = webdriver.Chrome()
driver.get('http://qa-assignment.oblakogroup.ru/board/:damir_nasretdinov#')

continue_link = driver.find_element_by_id('add_new_todo')
continue_link.click()

field = driver.find_element_by_id('select2-select_category-container')
field.click()

field = driver.find_element_by_css_selector('#todo_create_form > div > p:nth-child(1) > span > span.selection > span')
field.click()


field = driver.find_element_by_id('project_text')
field.clear()
field.send_keys('тест автомат')

continue_link = driver.find_element_by_id('submit_add_todo')
continue_link.click()

assert 'тест автомат' in driver.find_element_by_xpath('//*[@id="main_content_container"]/div[2]/div[6]/div').text

assert 'тест автомат' in driver.find_element_by_xpath('//*[@id="main_content_container"]/div[2]/div[1]/div').text


driver.quit()
