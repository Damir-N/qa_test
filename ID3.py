import selenium.webdriver.common.by
from behave import given, when, then


@given('открыть сайт')
def открыть_сайт(context):
    context.driver.get('открыть сайт http://qa-assignment.oblakogroup.ru/board/:damir_nasretdinov#')


@when('нажимает на кнопку добавить новую задачу')
def нажимает_на_кнопку_добавить_новую_задачу(context):
    context.driver.find_element_by_id('add_new_todo').click()


@then('открыть окно для добавления новой задачи')
def открыть_окно_для_добавления_новой_задачи(context):
    context.driver.find_element_by_id('select2-select_category-container')

@when('выбирать категорию Семья')
def выбирать_категорию_Семья(context):
    context.driver.find_element_by_css_selector('#todo_create_form > div > p:nth-child(1) > span > span.selection > span')

@when('в окно название задачи написать текст "тест автомат')
def написать_текст(context, текст_автомат):
    context.driver.find_element_by_id('project_text')
    send_keys('тест автомат')

@then('нажимает на кнопку "OK"')
def нажимает_кнопку(contex):
    contex.driver.find_element_by_id('submit_add_todo').click()

@then('проверка наличия текст автомат')
def проверка(context, текст_автомат):
    search_result_header = context.driver.find_element_by_xpath('//*[@id="main_content_container"]/div[2]/div[6]/div').text
    assert 'текст автомат' in проверка


@then("проверка наличия текст автомат в другой категории")
def проверка(context, текст_автомат):
    search_result_header = context.driver.find_element_by_xpath('//*[@id="main_content_container"]/div[2]/div[1]/div').text
    assert 'текст автомат' in проверка